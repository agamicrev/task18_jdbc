package com.agamicrev.service;

import com.agamicrev.DAO.implementation.LibraryHasBookDaoImpl;
import com.agamicrev.model.LibraryHasBook;
import java.sql.SQLException;
import java.util.List;

public class LibraryHasBookService {

  public List<LibraryHasBook> findAll() throws SQLException {
    return new LibraryHasBookDaoImpl().findAll();
  }

  public int create(LibraryHasBook entity) throws SQLException {
    return new LibraryHasBookDaoImpl().create(entity);
  }

  public int update(LibraryHasBook entity) throws SQLException {
    return new LibraryHasBookDaoImpl().update(entity);
  }

  public int delete(LibraryHasBook pk) throws SQLException {
    return new LibraryHasBookDaoImpl().delete(pk);
  }
}
