package com.agamicrev.DAO;

import com.agamicrev.model.LibraryHasBook;

public interface HasDAO extends GeneralDAO<LibraryHasBook, LibraryHasBook> {

}
