package com.agamicrev.DAO;

import com.agamicrev.model.BookEntity;
import java.sql.SQLException;
import java.util.List;

public interface BookDAO extends GeneralDAO<BookEntity, String> {

  List<BookEntity> findByName(String name) throws SQLException;

  List<BookEntity> findByCityNo(String cityNo) throws SQLException;
}
