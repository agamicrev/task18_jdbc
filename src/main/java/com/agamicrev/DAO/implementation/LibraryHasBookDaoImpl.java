package com.agamicrev.DAO.implementation;

import com.agamicrev.DAO.HasDAO;
import com.agamicrev.model.LibraryHasBook;
import com.agamicrev.persistant.ConnectionManager;
import com.agamicrev.transformer.Transformer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LibraryHasBookDaoImpl implements HasDAO {

  private static final String FIND_ALL = "SELECT * FROM library_has_book";
  private static final String CREATE = "INSERT library_has_book (library_id, book_id) VALUES (?, ?)";

  @Override
  public List<LibraryHasBook> findAll() throws SQLException {
    List<LibraryHasBook> has = new ArrayList<>();
    Connection connection = ConnectionManager.getConnection();
    try (Statement statement = connection.createStatement()) {
      try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
        while (resultSet.next()) {
          has.add((LibraryHasBook) new Transformer(LibraryHasBook.class)
              .fromResultSetToEntity(resultSet));
        }
      }
    }
    return has;
  }

  @Override
  public LibraryHasBook findById(LibraryHasBook pk_hasA) throws SQLException {
    return null;
  }

  @Override
  public int create(LibraryHasBook entity) throws SQLException {
    Connection conn = ConnectionManager.getConnection();
    try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
      ps.setInt(1, entity.getBook_id());
      ps.setInt(2, entity.getLibrary_id());
      return ps.executeUpdate();
    }
  }

  @Override
  public int update(LibraryHasBook entity) throws SQLException {
    return 0;
  }

  @Override
  public int delete(LibraryHasBook libraryHasBook) throws SQLException {
    return 0;
  }

}
