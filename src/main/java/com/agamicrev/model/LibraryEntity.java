package com.agamicrev.model;

import com.agamicrev.model.Annotation.Column;
import com.agamicrev.model.Annotation.PrimaryKey;
import com.agamicrev.model.Annotation.Table;

@Table(name = "library")
public class LibraryEntity {

  @PrimaryKey
  @Column(name = "id", length = 10)
  private String id;
  @Column(name = "name", length = 45)
  private String name;
  @Column(name = "books")
  private Integer books;
  @Column(name = "magazines")
  private Integer magazines;
  @Column(name = "popularity")
  private Double popularity;
  @Column(name = "city_id")
  private Integer city_id;

  public LibraryEntity() {
  }

  public LibraryEntity(String id, String name, Integer books, Integer magazines, Double popularity,
      Integer city_id) {
    this.id = id;
    this.name = name;
    this.books = books;
    this.magazines = magazines;
    this.popularity = popularity;
    this.city_id = city_id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getBooks() {
    return books;
  }

  public void setBooks(Integer books) {
    this.books = books;
  }

  public Integer getMagazines() {
    return magazines;
  }

  public void setMagazines(Integer magazines) {
    this.magazines = magazines;
  }

  public Double getPopularity() {
    return popularity;
  }

  public void setPopularity(Double popularity) {
    this.popularity = popularity;
  }

  public Integer getCity_id() {
    return city_id;
  }

  public void setCity_id(Integer city_id) {
    this.city_id = city_id;
  }

  @Override
  public String toString() {
    return String
        .format("%-11s %-15s %-5d %-5d %-15d %d", id, name, books, magazines, popularity, city_id);
  }
}
