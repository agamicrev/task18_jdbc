package com.agamicrev.model;

import com.agamicrev.model.Annotation.Column;
import com.agamicrev.model.Annotation.PrimaryKey;
import com.agamicrev.model.Annotation.Table;

@Table(name = "city")
public class CityEntity {

  @PrimaryKey
  @Column(name = "id", length = 5)
  private String id;
  @Column(name = "name", length = 15)
  private String name;

  public CityEntity() {
  }

  public CityEntity(String id, String name) {
    this.id = id;
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format("%-5s %-15s %s", id, name);
  }
}
