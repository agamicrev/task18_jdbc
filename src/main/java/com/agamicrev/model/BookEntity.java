package com.agamicrev.model;

import com.agamicrev.model.Annotation.Column;
import com.agamicrev.model.Annotation.PrimaryKey;
import com.agamicrev.model.Annotation.Table;

@Table(name = "book")
public class BookEntity {

  @PrimaryKey
  @Column(name = "id")
  private Integer id;
  @Column(name = "name_author", length = 45)
  private String name_author;
  @Column(name = "published", length = 4)
  private String published;
  @Column(name = "imdb_id", length = 5)
  private String imdb_id;

  public BookEntity() {
  }

  public BookEntity(Integer id, String name_author, String published, String imdb_id) {
    this.id = id;
    this.name_author = name_author;
    this.published = published;
    this.imdb_id = imdb_id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer empNo) {
    this.id = id;
  }

  public String getNameAuthor() {
    return name_author;
  }

  public void setNameAuthor(String name_author) {
    this.name_author = name_author;
  }

  public String getPublished() {
    return published;
  }

  public void setPublished(String published) {
    this.published = published;
  }

  public String getImdbId() {
    return imdb_id;
  }

  public void setImdbId(String deptNo) {
    this.imdb_id = imdb_id;
  }

  @Override
  public String toString() {
    return String.format("%-7d %-15s %-15s %s", id, name_author, published, imdb_id);
  }
}
