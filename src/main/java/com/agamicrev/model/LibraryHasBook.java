package com.agamicrev.model;

import com.agamicrev.model.Annotation.Column;
import com.agamicrev.model.Annotation.Table;

@Table(name = "library_has_book")
public class LibraryHasBook {

  @Column(name = "library_id")
  private Integer library_id;
  @Column(name = "book_id", length = 10)
  private Integer book_id;

  public LibraryHasBook() {
  }

  public LibraryHasBook(Integer library_id, Integer book_id) {
    this.library_id = library_id;
    this.book_id = book_id;
  }

  public Integer getLibrary_id() {
    return library_id;
  }

  public void setLibrary_id(Integer library_id) {
    this.library_id = library_id;
  }

  public Integer getBook_id() {
    return book_id;
  }

  public void setBook_id(Integer book_id) {
    this.book_id = book_id;
  }

  @Override
  public String toString() {
    return String.format("%-5d %-5d", getLibrary_id(), getBook_id());
  }
}
